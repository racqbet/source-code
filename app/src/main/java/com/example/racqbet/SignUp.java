package com.example.racqbet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SignUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //Call HomeScreen activity
        Button hs = findViewById(R.id.btnsigninup);
        hs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("Button Clicked");
                Intent hs = new Intent(getApplicationContext(), HomeScreen.class);
                startActivity(hs);
            }
        });
    }
}
